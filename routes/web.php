<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/video/create', [App\Http\Controllers\VideoController::class, 'create'])->name('video.create');
Route::get('/video', [App\Http\Controllers\VideoController::class, 'index'])->name('video.index');
Route::post('/video', [App\Http\Controllers\VideoController::class, 'store'])->name('video.store');
Route::get('/video/{id}', [App\Http\Controllers\VideoController::class, 'show'])->name('video.show');
Route::get('/video/{id}/edit', [App\Http\Controllers\VideoController::class, 'edit'])->name('video.edit');
Route::delete('/video/{id}', [App\Http\Controllers\VideoController::class, 'destroy'])->name('video.destroy');
Route::put('/video/{id}', [App\Http\Controllers\VideoController::class, 'update'])->name('video.update');
