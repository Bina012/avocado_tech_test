<?php

namespace App\Http\Controllers;

use App\Http\Requests\videoRequest;
use App\Models\video;
use App\Models\videoDetail;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['record'] = video::all();
        return view('video.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('video.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(videoRequest $request)
    {
        $request->request->all();
        $record = video::create($request->all());
        if ($record) {
            $videoDetails['video_id'] = $record->id;
            $detail_name = $request->detail_name;
            $detail_link = $request->detail_link;
            for ($i = 0; $i < count($detail_name); $i++) {
                $videoDetails['name'] = $detail_name[$i];
                $videoDetails['link'] = $detail_link[$i];
                videoDetail::create($videoDetails);
            }
            $request->session()->flash('success','Video Created Successfully.');
        }else{
            $request->session()->flash('error','Video Created failed.');
        }
        return redirect()->route('video.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\video  $video
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['record'] = video::find($id);
        return view('video.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['record'] = video::with("videoDetails")->find($id);
        if ($data['record']){
            return view('video.edit',compact('data'));
        }else{
            request()->session()->flash('error','Invalid Request');
            return redirect()->route('video.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(videoRequest $request,$id)
    {
        $data['record'] = video::find($id);
        $request->request->all();
        if ($data['record']->update($request->all())){
            $request->session()->flash('success','Video update successfully');
        }else{
            $request->session()->flash('error','Video updation failed');
        }
        return redirect()->route('video.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $data['record'] = video::find($id);
        $data['record']->videoDetails()->delete();
        $data['record']->delete();
        if ($data['record']){
            $request->session()->flash('success','Video delete success');
            return redirect()->route('video.index');
        }else{
            $request->session()->flash('error','Video deletion failed');
            return redirect()->route('video.index');
        }
    }
}
