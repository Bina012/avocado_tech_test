<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class videoDetail extends Model
{
    use HasFactory;
    protected $table = 'video_list';
    protected $fillable = ['video_id','name','link'];
}
