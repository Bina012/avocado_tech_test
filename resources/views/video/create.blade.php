@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="col-lg-12">
                    <a href="{{route('video.index')}}" class="btn btn-info">Video List</a>
                    <div class="card-header">Create video</div>
                </div>


                <div class="card-body">
                    {!! Form::open(['route' => 'video.store', 'method' => 'post']) !!}
                    <div class="form-group">
                        {!! Form::label('name', 'Name'); !!}
                        {!! Form::text('name', null,['class' => 'form-control']); !!}
                        @include('video.common.validation_field',['field' => 'name'])
                    </div>
                    <div class="form-group">
                        {!! Form::label('description', 'Description'); !!}
                        {!! Form::textarea('description', null,['class' => 'form-control','rows' => 2]); !!}
                        @include('video.common.validation_field',['field' => 'description'])
                    </div>
                    <div class="form-group">
                        {!! Form::label('active', 'Active'); !!}<br>
                        {!! Form::radio('active', 1); !!} Yes
                        {!! Form::radio('active', 0,true); !!} No
                        @include('video.common.validation_field',['field' => 'active'])
                    </div>
                    <br>
                    <br>
                    <h3>Video list</h3>
                    <table class="table table-striped table-bordered" id="add_video">
                        <tr>
                            <th>Name</th>
                            <th>Link</th>
                            <th>Action</th>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" name="detail_name[]" class="form-control" placeholder="Enter the name">
                            </td>
                            <td>
                                <input type="text" name="detail_link[]" class="form-control" placeholder="Enter the link">
                            </td>
                            <td>
                                <a class="btn btn-block btn-warning sa-warning remove_row"><i class="fa fa-trash">Delete</i></a>
                            </td>
                        </tr>
                    </table>
                    <button class="btn btn-info" type="button" id="addbutton" style="margin-bottom: 20px">
                        <i class="fa fa-plus"></i>
                        Add
                    </button>
                    <div class="form-group">
                        {!! Form::submit('Save video', ['class' => 'btn btn-info']); !!}
                        {!! Form::reset('Clear', ['class' => 'btn btn-danger']); !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript">
        var add_video= $("#add_video");
        var add = $("#addbutton");
        var x = 1;
        $(add).click(function (e) {
            e.preventDefault();
            var max_fields = 5;
            if (x < max_fields) {
                x++;
                $("#add_video tr:last").after(
                    '<tr>'+
                    '   <td> <input type="text" name="detail_name[]" class="form-control" placeholder="Enter the name">'+
                    '   </td>'+
                    '  <td><input type="text" name="detail_link[]" class="form-control" placeholder="Enter the link"></td>'+
                    '   <td>'+
                    '       <a class="btn btn-block btn-warning sa-warning remove_row"><i class="fa fa-trash">Delete</i></a>'+
                    '   </td>'+
                    '</tr>'
                );
            }else{
                alert('Maximum video Limit is 5');
            }
        });
        $(add_video).on("click", ".remove_row", function (e) {
            e.preventDefault();
            $(this).parents("tr").remove();
            x--;
        });
    </script>
    <script src="//cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('description');
    </script>
@endsection
