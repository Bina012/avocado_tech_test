@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="col-lg-12">
                    <a href="{{route('video.index')}}" class="btn btn-info">Back</a>
                    <div class="card-header">Show videos</div>

                </div>
                <div class="card-body">
                    <table class="table-bordered table">
                        <tr>
                            <th>Name</th>
                            <td>{{$data['record']->name}}</td>
                        </tr>
                        <tr>
                            <th>Description</th>
                            <td>{!! $data['record']->description !!}</td>
                        </tr>
                        <tr>
                            <th>Active</th>
                            <td>
                                @if($data['record']->active==1)
                                    <span class="text-success">Yes</span>
                                @else
                                    <span class="text-danger">No</span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Created at</th>
                            <td>{{$data['record']->created_at}}</td>
                        </tr>
                        <tr>
                            <th>Updated at</th>
                            <td>{{$data['record']->updated_at}}</td>
                        </tr>
                    </table>
                    <br>
                    <h4>Video Details</h4>
                    <table class="table table-bordered">
                        <tr>
                            <th>Name</th>
                            <th>Link</th>
                        </tr>
                        @foreach($data['record']->videoDetails as $detail)
                            <tr>
                                <td>{{$detail->name}}</td>
                                <td>{{$detail->link}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
