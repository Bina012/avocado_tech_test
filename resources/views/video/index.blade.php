@extends('layouts.app')
@section('css')
    <link   rel="stylesheet" href="//cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css"/>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="col-lg-12">
                    <a href="{{route('video.create')}}" class="btn btn-info">Video Create</a>
                    @include('video.common.flash_message')
                    <div class="card-header">List video</div>
                </div>
                <div class="card-body">
                    <table class="table-bordered table" id="table1">
                        <thead>
                        <tr>
                            <th>S.N</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        @foreach($data['record'] as $record)
                        <tbody>
                        <tr>
                            <td>{{$loop->index+1}}</td>
                            <td>{{$record->name}}</td>
                            <td>
                                <a href="{{route('video.show',$record->id)}}" class="btn btn-primary">View Details</a>
                                <a href="{{route('video.edit',$record->id)}}" class="btn btn-warning">Edit</a>
                                <form action="{{route('video.destroy',$record->id)}}" method="post">
                                    <input type="hidden" name="_method" value="DELETE">
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.js" ></script>
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready( function () {
            $('#table1').DataTable();
        } );
    </script>
@endsection
